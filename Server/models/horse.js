module.exports = function(sequelize, Sequelize) {
  const HorseSchema = sequelize.define('Horse', {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    title: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    image: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },
    rating: {
      type: Sequelize.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    ratingNumber: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
  });
  HorseSchema.associate = (models) => {
    models.Horse.belongsTo(models.User), {
      foreignKey: 'id',
    };
    models.Horse.hasMany(models.Comment), {
      foreignKey: 'id',
    };
    models.Horse.hasMany(models.Rating), {
      foreignKey: 'id',
    };
  };
  return HorseSchema;
};

