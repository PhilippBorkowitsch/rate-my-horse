module.exports = function(sequelize, Sequelize) {
  const CommentSchema = sequelize.define('Comment', {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    text: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  });
  CommentSchema.associate = (models) => {
    models.Comment.belongsTo(models.User), {
      foreignKey: 'id',
    };
    models.Comment.belongsTo(models.Horse), {
      foreignKey: 'id',
    };
  };

  return CommentSchema;
};
