module.exports = function(sequelize, Sequelize) {
  const RatingSchema = sequelize.define('Rating', {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    value: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
  });
  RatingSchema.associate = (models) => {
    models.Rating.belongsTo(models.User), {
      foreignKey: 'id',
      as: 'Ratings',
    };
    models.Rating.belongsTo(models.Horse), {
      foreignKey: 'id',
    };
  };
  return RatingSchema;
};
