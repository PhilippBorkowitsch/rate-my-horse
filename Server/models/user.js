module.exports = function(sequelize, Sequelize) {
  const UserSchema = sequelize.define('User', {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },
    email: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    salt: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  });
  UserSchema.associate = (models) => {
    models.User.hasMany(models.Horse), {
      foreignKey: 'id',
    };
    models.User.hasMany(models.Comment), {
      foreignKey: 'id',
    };
    models.User.hasMany(models.Rating), {
      foreignKey: 'id',
    };
  };
  return UserSchema;
};
