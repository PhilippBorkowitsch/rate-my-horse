import { HomePage } from './home.po';
import { browser, logging, ActionSequence } from 'protractor';
import { HorsePage } from './horse.po';

describe('workspace-project App', () => {
  let page: HomePage;

  beforeEach(() => {
    page = new HomePage();
    page.navigateTo();
  });

  it('should display a list of horses', () => {
    expect(page.getHorseCardElements().count()).toBeGreaterThanOrEqual(3);
  });

  it('should lead to register page', () => {
    page.getFooterRegisterButton().click();
    expect(browser.getCurrentUrl()).toContain('/register');
  });

  it('should login to test account Torben', () => {
    page.getLoginDropdown().click();
    page.getUsernameInputControl().sendKeys('Torben\ttest123');
    page.getLoginSubmit().click();
    expect(page.getAlert().getText()).toContain('Torben');
    expect(page.getProfileDropdown().getText()).toContain('Torben');
  });

  it('should log out Torben', () => {
    page.getProfileDropdown().click();
    page.getLogoutButton().click();
    expect(page.getLoginDropdown().getText()).toBe('Login');
  })

  it('should login to test account Jon', () => {
    page.getLoginDropdown().click();
    page.getUsernameInputControl().sendKeys('Jon\ttest123');
    page.getLoginSubmit().click();
    expect(page.getAlert().getText()).toContain('Jon');
    expect(page.getProfileDropdown().getText()).toContain('Jon');
  });

  it('should navigate to first horse page', () => {
    page.getFirstHorseThumbnail().click();
    expect(browser.getCurrentUrl()).toContain('/horse/1');
  })

  it('should log out Jon', () => {
    page.getProfileDropdown().click();
    page.getLogoutButton().click();
    expect(page.getLoginDropdown().getText()).toBe('Login');
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});

describe('horse display page', () => {
  let page: HorsePage;

  beforeEach(() => {
    page = new HorsePage();
    page.navigateTo();
  });

  it('should post a comment as Jon with text: ist mir doch egal', async () => {
    const homePage = new HomePage();
    homePage.getLoginDropdown().click();
    homePage.getUsernameInputControl().sendKeys('Jon\ttest123');
    homePage.getLoginSubmit().click();

    const numberOfComments = await page.getComments().count()
    page.getCommentInput().sendKeys('ist mir doch egal');
    page.getCommentSubmit().click();
    expect(page.getComments().count()).toBe(numberOfComments + 1);
    expect(page.getComments().last().getText()).toContain('ist mir doch egal');
    expect(page.getComments().last().getText()).toContain('Jon');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });

})