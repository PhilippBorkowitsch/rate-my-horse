import { browser, by, element } from 'protractor';

export class HorsePage {
    navigateTo() {
        return browser.get('/horse/1');
    }

    getCommentInput() {
        return element(by.css('app-comment-input input[type="text"]'));
    }

    getCommentSubmit() {
        return element(by.css('app-comment-input .btn'));
    }

    getComments() {
        return element.all(by.css('app-comment'));
    }
}