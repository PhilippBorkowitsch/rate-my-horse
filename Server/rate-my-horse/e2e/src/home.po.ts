import { browser, by, element } from 'protractor';

export class HomePage {
    navigateTo() {
        return browser.get('/');
    }

    getHorseCardElements() {
        return element.all(by.css('.card'));
    }

    getFooterRegisterButton() {
        return element(by.css('footer .btn'));
    }

    getLoginDropdown() {
        return element(by.css('.navbar .dropdown-toggle'));
    }

    getUsernameInputControl() {
        return element(by.css('app-login input[formcontrolname=username]'));
    }

    getLoginSubmit() {
        return element(by.css('app-login button'));
    }

    getLogoutButton() {
        return element(by.cssContainingText('.dropdown-item', 'Logout'));
    }

    getProfileDropdown() {
        return element(by.css('.navbar .dropdown-toggle'));
    }

    getAlert() {
        return element(by.css('.alert'));
    }
    
    getFirstHorseThumbnail() {
        return element.all(by.css('.card')).first();
    }
}