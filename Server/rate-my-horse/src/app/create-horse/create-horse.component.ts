import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Horse } from '@/_models';
import { HorseService, AlertService } from '@/_services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-horse',
  templateUrl: './create-horse.component.html',
  styleUrls: ['./create-horse.component.scss']
})
export class CreateHorseComponent implements OnInit {

 horseForm: FormGroup;
 horse: Horse;
 horseImage;

  constructor(
    private formBuilder: FormBuilder,
    private horseService: HorseService,
    private router: Router,
    private alertService: AlertService,
  ) { }

  ngOnInit() {
    this.horseForm = this.formBuilder.group({
      horseImage: ['', Validators.required],
      horseTitle: [''],
    });
  }

  onSubmit() {
    this.horseService.updateTitle(this.horseForm.controls.horseTitle.value, this.horse).subscribe();
    this.alertService.success('New horse created!', true);
    this.router.navigate(['horse', this.horse.id]);
  }

  onFileUpload() {
    if (this.horseImage) {
      this.horseService.createHorse(this.horseImage).subscribe((horse) => {
        this.horse = horse;
      });
    }
  }

  onFileChanged(event) {
    this.horseImage = event.target.files[0];    
  }

}
