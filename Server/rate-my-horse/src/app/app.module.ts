import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RatingModule, StarRatingComponent } from 'ng-starrating';
import { appRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AlertComponent } from './alert/alert.component';
import { HorseComponent } from './horse/horse.component';
import { HttpClientModule } from '@angular/common/http';
import { CommentComponent } from './horse/comment/comment.component';
import { CommentInputComponent } from './horse/comment-input/comment-input.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { HorseThumbComponent } from './home/horse-thumb/horse-thumb.component';
import { CreateHorseComponent } from './create-horse/create-horse.component';
import { RegisterComponent } from './register/register.component';
import { RatingComponent } from './horse/rating/rating.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AlertComponent,
    HorseComponent,
    CommentComponent,
    CommentInputComponent,
    HeaderComponent,
    HomeComponent,
    HorseThumbComponent,
    CreateHorseComponent,
    RegisterComponent,
    RatingComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    appRoutingModule,
    HttpClientModule,
    RatingModule,
    FormsModule,
    FontAwesomeModule,
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
