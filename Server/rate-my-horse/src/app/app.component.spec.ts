import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StarRatingComponent, RatingModule } from 'ng-starrating';
import { RatingComponent } from './horse/rating/rating.component';
import { CommentComponent } from './horse/comment/comment.component';
import { CommentInputComponent } from './horse/comment-input/comment-input.component';
import { HeaderComponent } from './header/header.component';
import { AlertComponent } from './alert/alert.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        RatingModule,
        HttpClientModule,
        FontAwesomeModule,
      ],
      declarations: [
        AppComponent,
        AlertComponent,
        LoginComponent,
        RatingComponent,
        CommentComponent,
        CommentInputComponent,
        HeaderComponent,
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
