import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HorseComponent } from './horse/horse.component';
import { HomeComponent } from './home/home.component';
import { CreateHorseComponent } from './create-horse/create-horse.component';
import { RegisterComponent } from './register/register.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'create', component: CreateHorseComponent },
  { path: 'login', component: LoginComponent },
  { path: 'horse/:horseId', component: HorseComponent },

  { path: '*', redirectTo: '' },
];

export const appRoutingModule = RouterModule.forRoot(routes);
