import { Component, OnInit } from '@angular/core';
import { HorseService } from '@/_services';
import { Horse } from '@/_models';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  horses: Observable<Horse[]>;

  constructor(
    private horseService: HorseService
  ) { }

  ngOnInit() {
    this.horses = this.horseService.getAll();
  }

}
