import { Component, OnInit, Input } from '@angular/core';
import { Horse } from '@/_models';

@Component({
  selector: 'app-horse-thumb',
  templateUrl: './horse-thumb.component.html',
  styleUrls: ['./horse-thumb.component.scss']
})
export class HorseThumbComponent implements OnInit {
  
  @Input()
  horse: Horse;

  constructor() { }

  ngOnInit() {
  }

}
