import { Component, OnInit } from '@angular/core';
import { User } from '@/_models';
import { AuthenticationService, AlertService } from '@/_services';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { faHorseHead } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  loggedInUser: Observable<string>;
  loading = true;
  faHorseHead = faHorseHead;
  constructor(
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
  }

  ngOnInit() {
    this.loggedInUser = this.authenticationService.whoIsLoggedIn().pipe(map((user) => {
      return user?user.name:undefined;
    }));
  }

  onLogin(user: User) {
    console.log(user);

    this.authenticationService.refreshLoggedIn();
  }

  logout() {
    this.alertService.clear();
    this.authenticationService.logout();
    this.authenticationService.refreshLoggedIn();
  }

}
