import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService, UserService } from '@/_services';
import { Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private userServive: UserService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      userEmail: ['', [Validators.required, Validators.email]],
      userPassword: ['', Validators.required],
      userPasswordControl: ['', Validators.required],
      userName: ['', Validators.required],
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    this.alertService.clear();

    if (this.registerForm.invalid) {
      return;
    } else if (!this.checkMatchingPasswords()) {
      this.alertService.error('Passwords not matching');
      return;
    }
    this.userServive.registerUser(
      this.f.userName.value,
      this.f.userEmail.value,
      this.f.userPassword.value
    ).subscribe((user) => {
      this.alertService.success('Welcome! Login to post and comment', true);
      this.router.navigate(['']);
    }, (error) => {
      this.alertService.error('User already exists');
    });
  }

  checkMatchingPasswords(): boolean {
    return this.f.userPassword.value === this.f.userPasswordControl.value;
  }
}
