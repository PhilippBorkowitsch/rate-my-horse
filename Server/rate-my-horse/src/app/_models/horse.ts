import { User } from './user';
import { HorseComment } from './horseComment';
import { Rating } from './rating';

export class Horse {
    constructor(data) {
        this.id = data.id;
        this.title = data.title;
        this.image = data.image;
        this.rating = data.rating;
        this.ratingNumber = data.ratingNumber;
        this.user = new User(data.User);
        if (data.Comments) {
            this.comments = data.Comments.map((comment) => {
                return new HorseComment(comment);
            });
        }
        if (data.Ratings) {
            this.ratings = data.Ratings.map((rating) => {
                return new Rating(rating);
            });
        }
    }
    id: number;
    title: string;
    image: string;
    rating: number;
    ratingNumber: number;
    user: User;
    comments: HorseComment[];
    ratings: Rating[];

    addComment = (comment: HorseComment) => {
        this.comments.push(comment);
    }
}
