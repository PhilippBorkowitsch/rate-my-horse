import { Horse } from './horse';
import { User } from './user';

export class Rating {
    constructor(data) {
        this.horseId = data.HorseId;
        this.userId = data.UserId;
        this.value = data.value;
    }
    horseId: number;
    userId: number;
    value: number;
}
