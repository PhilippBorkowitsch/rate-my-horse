import { Horse } from './horse';
import { User } from './user';

export class HorseComment {
    constructor(data) {
        this.id = data.id;
        this.text = data.text;
        this.horseId = data.HorseId;
        this.user = new User(data.User);
    }
    id: number;
    text: string;
    horseId: number;
    user: User;
}
