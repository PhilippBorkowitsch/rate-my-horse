import { Horse } from './horse';
import { HorseComment } from './horseComment';
import { Rating } from './rating';

export class User {
    constructor(data) {
        this.id = data.id;
        this.name = data.name;
        this.email = data.email;
        if (data.Horses) {
            this.horseIds = data.Horses.map((horse) => {
                return horse.id;
            });
        }
        if (data.Ratings) {
            this.ratings = data.Ratings.map((rating) => {
                return new Rating(rating);
            });
        }
    }
    id: number;
    name: string;
    email: string;
    horseIds: number[];
    comments: HorseComment[];
    ratings: Rating[];
}
