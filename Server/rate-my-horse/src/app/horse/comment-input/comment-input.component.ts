import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthenticationService, CommentService, AlertService } from '@/_services';
import { User, Horse, HorseComment } from '@/_models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-comment-input',
  templateUrl: './comment-input.component.html',
  styleUrls: ['./comment-input.component.scss']
})
export class CommentInputComponent implements OnInit {
  commentForm: FormGroup;
  currentUser: User;
  loggedIn = false;
  submitted = false;

  @Input()
  horse: Horse;

  @Output()
  notifyCommentCreated: EventEmitter<HorseComment> = new EventEmitter<HorseComment>();

  constructor(
    private formBuilder: FormBuilder,
    private commentService: CommentService,
    private alertService: AlertService,
  ) {
  }

  get f() { return this.commentForm.controls; }

  ngOnInit() {
    this.commentForm = this.formBuilder.group({
      commentText: ['', Validators.required]
    });
  }

  onSubmit() {
    this.submitted = true;
    this.alertService.clear();
    if (this.commentForm.invalid) {
      return;
    }
    this.commentService.postComment(this.horse, this.f.commentText.value)
      .subscribe((comment: HorseComment) => {
        this.notifyCommentCreated.emit(comment);
        this.commentForm.reset();
        this.loggedIn = true;
      }, (error) => {
        if (error.status === 401) {
          this.alertService.error('Please log in to comment');
          this.loggedIn = false;
        }
      });
  }

}
