import { Component, OnInit } from '@angular/core';
import { Horse, HorseComment } from '@/_models';
import { HorseService } from '@/_services/horse.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-horse',
  templateUrl: './horse.component.html',
  styleUrls: ['./horse.component.scss']
})
export class HorseComponent implements OnInit {
  horseId: number;
  horse: Horse;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private horseService: HorseService,
  ) { }

  ngOnInit() {
    this.loadHorses();
  }

  onCommentCreated(comment: HorseComment) {
    this.horse.addComment(comment);
  }

  loadHorses() {
    this.horseId = parseInt(this.route.snapshot.paramMap.get('horseId'), 10);

    this.horseService.getById(this.horseId)
    .subscribe((horse) => {
      this.horse = horse;
    });
  }
}
