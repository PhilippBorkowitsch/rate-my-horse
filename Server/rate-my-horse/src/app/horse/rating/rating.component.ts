import { Component, OnInit, Input } from '@angular/core';
import { StarRatingComponent } from 'ng-starrating';
import { Horse } from '@/_models';
import { HorseService, AlertService, AuthenticationService } from '@/_services';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

  @Input()
  horse: Horse;

  readOnly: boolean;

  constructor(
    private horseService: HorseService,
    private alertService: AlertService,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit() {
    this.authenticationService.whoIsLoggedIn().subscribe((user) => {
      if (!user) {
        this.readOnly = false;
      } else if (user.ratings) {
        user.ratings.forEach(rating => {
          console.log(rating);

          if (rating.horseId === this.horse.id) {
            this.readOnly = true;
          }
        });
      }
    });
  }

  onRate($event: {oldValue: number, newValue: number, starRating: StarRatingComponent}) {
    this.horseService.rateHorse(
      this.horse.id,
      $event.newValue,
    ).subscribe((horse) => {
      this.horse = horse;
      $event.starRating.value = this.horse.rating;
      $event.starRating.readonly = true;
    }, (error) => {
      if (error.status === 401) {
        this.alertService.error('Please log in to rate');
      } else if (error.status === 400) {
        this.alertService.error('You already rated this horse');
      }
      $event.starRating.value = $event.oldValue;
    });
  }
}
