import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, Horse, HorseComment } from '@/_models';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  postComment(horse: Horse, text: string): Observable<HorseComment> {
    return this.http.post(`/api/comment/${horse.id}`, {
      commentText: text,
    })
    .pipe(map((data: Object) => {
      return new HorseComment(data);
    }));
  }
}
