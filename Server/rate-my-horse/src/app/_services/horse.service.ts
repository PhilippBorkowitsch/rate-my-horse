import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';

import { Horse } from '@/_models';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HorseService {

  constructor(private http: HttpClient) { }

  createHorse(horseImage: File): Observable<Horse> {
    const formData = new FormData();
    formData.append('picturefile', horseImage);
    return this.http.post('/api/horse/', formData)
      .pipe(map((horse) => {
        console.log(horse);
        return new Horse(horse);
      }));
  }

  updateTitle(title: string, horse: Horse) {
    return this.http.put(`/api/horse/${horse.image}`, {
      horseTitle: title,
    }).pipe(map((data) => {
      return new Horse(data);
    }));
  }

  getAll(): Observable<Horse[]> {
    return this.http.get(`/api/horse/`)
      .pipe(map((data: []) => {
        return data.map((horse) => {
          return new Horse(horse);
      });
    }));
  }

  getById(horseId: number): Observable<Horse> {
    return this.http.get(`/api/horse/${horseId}`)
      .pipe(map((data) => {
        return new Horse(data);
      }));
  }

  getByUserId(userId: number): Observable<Horse> {
    return this.http.get(`/api/horse/user/${userId}`)
    .pipe(map((data) => {
      return new Horse(data);
    }));
  }

  rateHorse(horseId: number, ratingValue: number): Observable<Horse> {
    return this.http.post(`/api/horse/${horseId}`, {
      ratingValue,
    }).pipe(map((data) => {
      return new Horse(data);
    }));
  }
}
