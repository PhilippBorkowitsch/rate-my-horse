import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { User } from '@/_models';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
  ) { }

  registerUser(userName: string, userEmail: string, userPassword){
    return this.http.post('/api/user/', {
      userName: userName,
      userEmail: userEmail,
      userPassword: userPassword,
    }).pipe(map((user) => {
      return new User(user);
    }))
  }
}
