import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '@/_models';
import { map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  refresh = new BehaviorSubject(null);

  constructor(private http: HttpClient) {
  }

  public whoIsLoggedIn(): Observable<User> {
    return this.refresh.pipe(switchMap(() => {
      return this.http.get('/api/login/')
      .pipe(map((response) => {
        if (response) {
          return new User(response);
        } else {
          return undefined;
        }
      }));
    }));
  }

  public refreshLoggedIn() {
    this.refresh.next(null);
  }

  login(username, password) {
    return this.http.post('/api/login/', {
      userName: username,
      userPassword: password,
    })
      .pipe(map(response => {
        return new User(response);
    }));
  }

  logout() {
    return this.http.delete('/api/login/').subscribe();
  }

}
