import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CommentService } from '@/_services';
import { Horse } from '@/_models';

fdescribe('CommentService', () => {
    let injector: TestBed;
    let service: CommentService;
    let httpMock: HttpTestingController;
    
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [CommentService]
      });
      injector = getTestBed();
      service = injector.get(CommentService);
      httpMock = injector.get(HttpTestingController);
    });
    
    it('should create a comment', () => {
      const dummyComment = {
        id: 3,
        text: 'berry nice',
        HorseId: 1,
        User: {
            id: 1,
            name: 'torbina',
            email: 'torbina@gmail.com',
        }
      }

      const dummyHorse = {
        id: 2,
        title: 'Gertrude',
        image: 'SHHnnh4x',
        User: {
            id: 2,
            name: 'juergen',
            email: 'juergen.wuergen@bb.b'
        },
        Comments: [{
            id: 2,
            text: 'noice',
            HorseId: 1,
            User: {
                id: 1,
                name: 'torbina',
                email: 'torbina@gmail.com'
            }
        }],
        Ratings: [],
        rating: 0,
        ratingNumber: 0,
    }
      
      service.postComment(new Horse(dummyHorse), 'berry nice').subscribe((comment) => {
        expect(comment.id).toEqual(3);
        expect(comment.user.email).toEqual('torbina@gmail.com');
      })
  
      const req = httpMock.expectOne(`/api/comment/2`);
      expect(req.request.url).toBe(`/api/comment/2`);
      expect(req.request.method).toBe("POST");
      req.flush(dummyComment);
    })

    afterEach(() => {
      httpMock.verify();
    });
});