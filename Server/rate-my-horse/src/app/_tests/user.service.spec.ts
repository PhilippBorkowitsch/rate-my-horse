import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UserService } from '@/_services';

fdescribe('UserService', () => {
    let injector: TestBed;
    let service: UserService
    let httpMock: HttpTestingController;
    
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [UserService]
      });
      injector = getTestBed();
      service = injector.get(UserService);
      httpMock = injector.get(HttpTestingController);
    });
    
    it('should register a user', () => {
      const dummyUser = {
        id: 1,
        name: 'torbina',
        email: 'torbina@gmail.com'
      }
      
      service.registerUser('torbina', 'torbina@gmail.com', 'test123').subscribe((user) => {
        expect(user.name).toEqual('torbina');
        expect(user.email).toEqual('torbina@gmail.com');
      })
  
      const req = httpMock.expectOne(`/api/user/`);
      expect(req.request.method).toBe("POST");
      req.flush(dummyUser);
    })

    afterEach(() => {
      httpMock.verify();
    });
});