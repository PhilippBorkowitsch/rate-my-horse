import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HorseService } from '@/_services';
import { Horse } from '@/_models';


fdescribe('HorseService', () => {
  let injector: TestBed;
  let service: HorseService
  let httpMock: HttpTestingController;
  
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HorseService]
    });
    injector = getTestBed();
    service = injector.get(HorseService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should get all horses', () => {
      const dummyHorses = [
          {
              id: 1,
              title: 'nice horse',
              image: 'p_3ddGYL',
              User: {
                  id: 1,
                  name: 'torbina',
                  email: 'torbina@gmail.com'
              },
              Comments: [{
                  id: 1,
                  text: 'beautiful',
                  HorseId: 1,
                  User: {
                      id: 2,
                      name: 'juergen',
                      email: 'juergen.wuergen@bb.b'
                  }
              }],
              Ratings: [],
              rating: 0,
              ratingNumber: 0,
          },
          {
            id: 2,
            title: 'Gertrude',
            image: 'SHHnnh4x',
            User: {
                id: 2,
                name: 'juergen',
                email: 'juergen.wuergen@bb.b'
            },
            Comments: [{
                id: 2,
                text: 'noice',
                HorseId: 1,
                User: {
                    id: 1,
                    name: 'torbina',
                    email: 'torbina@gmail.com'
                }
            }],
            Ratings: [],
            rating: 0,
            ratingNumber: 0,
        },
      ]
      service.getAll().subscribe((horses) => {
        expect(horses.length).toBe(2);
        const fakeHorses = dummyHorses.map((horse) => {
            return new Horse(horse)
        })      
        expect(horses[0].id).toBe(1);
        expect(horses[1].id).toBe(2);
      })

      const req = httpMock.expectOne(`/api/horse/`);
      expect(req.request.method).toBe("GET");
      req.flush(dummyHorses);
  })

  it('should get one specific horse', () => {
    const dummyHorse = {
            id: 1,
            title: 'nice horse',
            image: 'p_3ddGYL',
            User: {
                id: 1,
                name: 'torbina',
                email: 'torbina@gmail.com'
            },
            Comments: [{
                id: 1,
                text: 'beautiful',
                HorseId: 1,
                User: {
                    id: 2,
                    name: 'juergen',
                    email: 'juergen.wuergen@bb.b'
                }
            }],
            Ratings: [],
            rating: 0,
            ratingNumber: 0,
        }
    
    service.getById(1).subscribe((horse) => {
      expect(horse.title).toBe('nice horse');
      expect(horse.user.name).toBe('torbina');
    })

    const req = httpMock.expectOne(`/api/horse/1`);
    expect(req.request.url).toBe(`/api/horse/1`);
    expect(req.request.method).toBe("GET");
    req.flush(dummyHorse);
  })
  
  it('should create a horse', () => {
    const dummyHorse = {
            id: 1,
            title: '',
            image: 'p_3ddGYL',
            User: {
                id: 1,
                name: 'torbina',
                email: 'torbina@gmail.com'
            },
            Comments: [],
            Ratings: [],
            rating: 0,
            ratingNumber: 0,
        }
    
    service.createHorse(null).subscribe((horse) => {
      expect(horse.title).toBe('');
      expect(horse.comments).toEqual([]);
      expect(horse.user.name).toBe('torbina');
    })

    const req = httpMock.expectOne(`/api/horse/`);
    expect(req.request.method).toBe("POST");
    req.flush(dummyHorse);
  })

  afterEach(() => {
    httpMock.verify();
  });
});