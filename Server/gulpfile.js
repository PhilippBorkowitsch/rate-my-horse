const gulp = require('gulp');
const exec = require('gulp-exec');
const open = require('gulp-open');
const eslint = require('gulp-eslint');
const {src, series, parallel} = require('gulp');


gulp.task('installFrontend', () => {
  return gulp.src('.')
      .pipe(exec('cd ./rate-my-horse && npm install'))
      .pipe(exec.reporter());
});

gulp.task('buildFrontend', () => {
  return gulp.src('.')
      .pipe(exec('cd ./rate-my-horse && ng build'))
      .pipe(exec.reporter());
});

gulp.task('runBackend', () => {
  return gulp.src('.')
      .pipe(exec('npm run dev'))
      .pipe(exec.reporter());
});

gulp.task('openInBrowser', () => {
  return gulp.src(__filename)
      .pipe(open({uri: 'http://localhost:3000/'}));
});

gulp.task('e2eTestFrontend', () => {
  return gulp.src('.')
      .pipe(exec('cd ./rate-my-horse && ng e'))
      .pipe(exec.reporter());
});

gulp.task('unitTestFrontend', () => {
  return gulp.src('.')
      .pipe(exec('cd ./rate-my-horse && ng test'))
      .pipe(exec.reporter());
});

gulp.task('eslint', () => {
  return src(['models/*.js', 'routes/api/*.js'])
      .pipe(eslint())
      .pipe(eslint.format())
      .pipe(eslint.failAfterError());
});

const frontend = series('installFrontend', 'buildFrontend');
const backend = series('runBackend');
const frontendTesting = series('e2eTestFrontend', 'unitTestFrontend');
const frontendWhole = series(frontend, 'openInBrowser', frontendTesting);
const buildAndRun = parallel(backend, frontendWhole);


exports.default = series('eslint', buildAndRun);
