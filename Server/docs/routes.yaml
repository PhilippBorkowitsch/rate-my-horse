openapi: 3.0.0
info:
  title: RateMyHorse
  version: 1.0.0
  description: RateMyHorse is an awsome image sharing site to get your horse the attention it really needs. This documenation provides information for some of the existing REST-server routes.
servers:
  - url: "http://localhost:3000/api"
    description: test build server
tags:
  - name: user
    description: Everything related to User
  - name: comment
    description: Posting and getting horse comments
  - name: horse
    description: This is what the site is all about
  - name: rating
    description: The thing Users should do
paths:
  /user/:
    post:
      tags:
        - user
      summary: Register a new User
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                userName:
                  type: string
                userEmail:
                  type: string
                userPassword:
                  type: string
            examples:
              user:
                summary: An example of a cat
                value:
                  userName: Max
                  userEmail: max@muster.com
                  userPassword: test123
      responses:
        '200':
          description: User created.
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                  name:
                    type: string
                  email:
                    type: string
        '400':
          description: internal server error
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
        '401':
          description: wrong user or password given
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
        '405':
          description: wrong email format
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
  /comment/:horseId:
    post:
      tags:
        - comment
      summary: Post a new comment
      parameters:
        - name: horseId
          in: path
          description: ID of horse which the comment belongs to
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                text:
                  type: string
            examples:
              comment:
                summary: An example of a comment
                value:
                  text: Very nice horse! 😀
      responses:
        '200':
          description: Comment created.
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                  text:
                    type: string
                  user:
                    type: object
                    properties:
                      id:
                        type: integer
                      name:
                        type: string
                      email:
                        type: string
                  horse:
                    type: object
                    properties:
                      id:
                        type: integer
                      title:
                        type: string
                      image:
                        type: string
                      rating:
                        type: number
                      ratingNumber:
                        type: integer
                      createdAt:
                        type: string
                      updatedAt:
                        type: string
        '500':
          description: internal server error
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
        '401':
          description: No user logged in.
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
  /horse/:
    get:
      tags: 
        - horse
      summary: Get all Horses
      responses:
        '200':
          description: Horses returned
          content:
            application/json:
              schema:
                type: object
                properties:
                  "":
                    type: array
                    items:
                      type: object
                      properties:
                        id:
                          type: integer
                        title:
                          type: string
                        image:
                          type: string
                          description: file name of horse image
                        rating:
                          type: number
                          description: rating score from 0 to 5
                        ratingNumber:
                          type: integer
                          description: number of ratings
                        user:
                          type: object
                          description: user who posted the horse
                          properties:
                            id:
                              type: integer
                            name:
                              type: string
                            email:
                              type: string
                        comments:
                            type: array
                            items:
                                type: object
                                properties:
                                  text:
                                    type: string
                                  user:
                                    type: object
                                    description: user who posted the comment
                                    properties:
                                      id:
                                        type: integer
                                      name:
                                        type: string
                                      email:
                                        type: string
                                  horseId:
                                    type: integer
                                  createdAt:
                                    type: string
                                  updatedAt:
                                    type: string
                        ratings:
                          type: object
                          properties:
                            value:
                              type: number
                              description: score of this rating
                            user:
                              type: object
                              description: user who rated
                              properties:
                                id:
                                  type: integer
                                name:
                                  type: string
                                email:
                                  type: string
                                createdAt:
                                  type: string
                                updatedAt:
                                  type: string   
                        createdAt:
                          type: string
                        updatedAt:
                          type: string
        '500':
          description: internal server error
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
    post:
      tags:
        - horse
      summary: Post a new Horse
      requestBody:
        content:
          multipart/form-data:
            schema:
              properties:
                file:
                  type: string
                  description: png image of horse
      responses:
        '200':
          description: Horse created.
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                  title:
                    type: string
                    description: should always be empty
                  image:
                    type: string
                    description: file name of horse image
                  rating:
                    type: number
                    description: should be 0
                  ratingNumber:
                    type: integer
                    description: should be 0
                  user:
                    type: object
                    description: user who posted the horse
                    properties:
                      id:
                        type: integer
                      name:
                        type: string
                      email:
                        type: string
                  comments:
                      type: array
                      description: should be empty
                  createdAt:
                    type: string
                  updatedAt:
                    type: string
        '400':
          description: no image was sent
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
        '401':
          description: no user logged in
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
        '500':
          description: internal server error
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
  /horse/:horseid:
    get:
      tags:
        - horse
      summary: Get specific horse
      parameters:
        - name: horseId
          in: path
          description: ID of horse to get
          required: true
          schema:
            type: string
      responses:
        '200':
          description: horse returned
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                  title:
                    type: string
                    description: title of horse 
                  image:
                    type: string
                    description: file name of horse image
                  rating:
                    type: number
                    description: rating score from 0 to 5
                  ratingNumber:
                    type: integer
                    description: number of ratings
                  user:
                    type: object
                    description: user who posted the horse
                    properties:
                      id:
                        type: integer
                      name:
                        type: string
                      email:
                        type: string
                  comments:
                      type: array
                      items:
                          type: object
                          properties:
                            text:
                              type: string
                            user:
                              type: object
                              description: user who posted the comment
                              properties:
                                id:
                                  type: integer
                                name:
                                  type: string
                                email:
                                  type: string
                            horseId:
                              type: integer
                            createdAt:
                              type: string
                            updatedAt:
                              type: string
                  ratings:
                    type: object
                    properties:
                      value:
                        type: number
                        description: score of this rating
                      user:
                        type: object
                        description: user who rated
                        properties:
                          id:
                            type: integer
                          name:
                            type: string
                          email:
                            type: string
                          createdAt:
                            type: string
                          updatedAt:
                            type: string
                  createdAt:
                    type: string
                  updatedAt:
                    type: string
        '500':
          description: internal server error
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
    post:
      tags:
        - horse
        - rating
      summary: Post a rating
      parameters:
        - name: horseId
          in: path
          description: ID of horse to post rating to
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                ratingValue:
                  type: number
      responses:
        '200':
          description: rating posted
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                  title:
                    type: string
                    description: title of horse 
                  image:
                    type: string
                    description: file name of horse image
                  rating:
                    type: number
                    description: updated rating score from 0 to 5
                  ratingNumber:
                    type: integer
                    description: updated number of ratings
                  user:
                    type: object
                    description: user who posted the horse
                    properties:
                      id:
                        type: integer
                      name:
                        type: string
                      email:
                        type: string
                  comments:
                      type: array
                      items:
                          type: object
                          properties:
                            text:
                              type: string
                            user:
                              type: object
                              description: user who posted the comment
                              properties:
                                id:
                                  type: integer
                                name:
                                  type: string
                                email:
                                  type: string
                            horseId:
                              type: integer
                            createdAt:
                              type: string
                            updatedAt:
                              type: string
                  ratings:
                    type: object
                    properties:
                      value:
                        type: number
                        description: score of this rating
                      user:
                        type: object
                        description: user who rated
                        properties:
                          id:
                            type: integer
                          name:
                            type: string
                          email:
                            type: string
                          createdAt:
                            type: string
                          updatedAt:
                            type: string
                  createdAt:
                    type: string
                  updatedAt:
                    type: string
        '401':
          description: no user logged in
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
        '500':
          description: internal server error
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
  /login/:
    post:
      tags:
        - user
      summary: User Login
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                userName:
                  type: string
                userPassword:
                  type: string
      responses:
        '200':
          description: user was logged in
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                  email:
                    type: string
                  name:
                    type: string
        '401':
          description: wrong username or password
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
        '500':
          description: internal server error
          content:
            application/json:
              schema:
                type: object
                properties:
                  err:
                    type: string
                    description: error message
    get:
      summary: Check Currently Logged In
      tags:
        - user
      responses:
        '200':
          description: response contains logged in user or undefined
          content:
            application/json:
              schema:
                type: object
                properties:
                  id:
                    type: integer
                  name:
                    type: string
                  email:
                    type: string
                  horses:
                    type: array
                    items:
                      type: object
                      properties:
                        id:
                          type: integer
                        title:
                          type: string
                        image:
                          type: string
                          description: file name of horse image
                        rating:
                          type: number
                          description: rating score from 0 to 5
                        ratingNumber:
                          type: integer
                          description: number of ratings
                        userId:
                          type: integer
                        createdAt:
                          type: string
                        updatedAt:
                          type: string
                  comments:
                    type: array
                    items:
                      type: object
                      properties:
                        text:
                          type: string
                        userId:
                          type: integer
                        horseId:
                          type: integer
                        createdAt:
                          type: string
                        updatedAt:
                          type: string
                  ratings:
                    type: array
                    items:
                      type: object
                      properties:
                        value:
                          type: integer
                        horseId:
                          type: integer
    delete:
      tags:
        - user
      summary: User Logout
      responses:
        '200':
          description: user logged out
