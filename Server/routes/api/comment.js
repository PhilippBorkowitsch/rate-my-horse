// eslint-disable-next-line new-cap
const router = require('express').Router();

const Comment = require('../../models').Comment;
const User = require('../../models').User;
const Horse = require('../../models').Horse;


const auth = require('./auth');

router.post('/:horseId', [auth.checkIfLoggedIn], (req, res) => {
  Comment.create({
    text: req.body.commentText,
    HorseId: req.params.horseId,
    UserId: req.session.userId,
  }).then((comment) => {
    return Comment.findOne({
      where: {
        id: comment.id,
      },
      include: [{
        model: User,
        attributes: ['id', 'email', 'name'],
      }, {
        model: Horse,
      }],
    });
  }).then((comment) => {
    res
        .status(200)
        .json((comment));
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  });
});

router.get('/horse/:horseId', (req, res) => {
  Comment.findAll({
    where: {
      HorseId: req.params.horseId,
    },
    include: [{
      model: User,
      attributes: ['id', 'name', 'email'],
    }, {
      model: Horse,
    }],
  }).then((comments) => {
    res
        .status(200)
        .json(comments);
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  });
});

router.get('/user/:userId', (req, res) => {
  Comment.findAll({
    where: {
      UserId: req.params.userId,
    },
    include: [{
      model: User,
      attributes: ['id', 'name', 'email'],
    }, {
      model: Horse,
    }],
  }).then((comments) => {
    res
        .status(200)
        .json(comments);
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  });
});

router.put('/:commentId', (req, res) => {
  Comment.update({
    text: req.body.commentText,
  }, {
    where: {
      id: req.params.commentId,
    },
  }).then((result) => {
    res
        .status(200)
        .json(result);
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  });
});

router.delete('/:commentId', (req, res) => {
  Comment.destroy({
    where: {
      id: req.params.commentId,
    },
  }).then((result) => {
    res
        .status(200)
        .json(result);
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  }); ;
});

module.exports = router;
