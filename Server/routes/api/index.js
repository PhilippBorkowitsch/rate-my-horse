// eslint-disable-next-line new-cap
const router = require('express').Router();

router.use('/user', require('./user'));
router.use('/horse', require('./horse'));
router.use('/login', require('./login'));
router.use('/comment', require('./comment'));

module.exports = router;
