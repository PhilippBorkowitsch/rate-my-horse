// eslint-disable-next-line new-cap
const router = require('express').Router();
const auth = require('./auth');
const User = require('../../models').User;
const Horse = require('../../models').Horse;
const Rating = require('../../models').Rating;
const Comment = require('../../models').Comment;

const crypto = require('crypto');

router.post('/', [auth.checkIfUserExists], (req, res) => {
  User.findOne({
    where: {
      name: req.body.userName,
    },
  }).then((user) => {
    const hashedPassword = crypto.pbkdf2Sync(req.body.userPassword,
        user.salt, 10000, 512, 'sha512')
        .toString('hex');
    if (user.password == hashedPassword) {
      req.session.loggedIn = true;
      req.session.userName = req.body.userName;
      req.session.userId = user.id;
      res
          .status(200)
          .json({
            'id': user.id,
            'email': user.email,
            'name': user.name,
          });
    } else {
      res
          .status(401)
          .json({
            err: 'Wrong username or password',
          });
    }
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  });
});

router.get('/', (req, res) => {
  if (req.session.loggedIn == true) {
    User.findOne({
      where: {
        id: req.session.userId,
      },
      attributes: ['id', 'name', 'email'],
      include: [{
        model: Horse,
      },
      {
        model: Comment,
      }, {
        model: Rating,
        attributes: ['HorseId', 'value'],
      }],
    }).then((user) => {
      res
          .status(200)
          .json(user);
    });
  } else {
    res
        .status(200)
        .json(undefined);
  }
});

router.delete('/', (req, res) => {
  req.session.loggedIn = false;
  req.session.userName = undefined;
  req.session.userId = undefined;
  res
      .status(200)
      .json();
});

module.exports = router;
