// eslint-disable-next-line new-cap
const router = require('express').Router();
const User = require('../../models').User;
const Horse = require('../../models').Horse;
const Comment = require('../../models').Comment;
const Rating = require('../../models').Rating;
const crypto = require('crypto');
const auth = require('./auth');

router.post('/',
    [auth.checkIfFullUserDataProvided, auth.checkIfEmailHasValidFormat],
    (req, res) => {
      const salt = crypto.randomBytes(16).toString('hex');
      const hashedPassword = crypto.pbkdf2Sync(req.body.userPassword, salt,
          10000, 512, 'sha512')
          .toString('hex');

      return User.create({
        name: req.body.userName,
        email: req.body.userEmail,
        password: hashedPassword,
        salt: salt,
      }).then((user) => {
        return User.findOne({
          where: {
            id: user.id,
          },
          attributes: ['id', 'name', 'email'],
          include: [{
            model: Horse,
          },
          {
            model: Comment,
          }, {
            model: Rating,
            attributes: ['HorseId', 'value'],
          }],
        });
      }).then((user) => {
        res
            .status(200)
            .json({
              name: user.name,
              email: user.email,
            });
      }).catch((err) => {
        res
            .status(500)
            .json({
              err: err.message,
            });
      });
    });

router.get('/:userId/name', (req, res) => {
  User.findOne({
    where: {
      id: req.params.userId,
    },
  }).then((user) => {
    if (user) {
      res
          .status(200)
          .json({
            name: user.name,
          });
    } else {
      res
          .status(400)
          .json({
            err: 'No such user',
          });
    }
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  });
});

router.get('/:userId',
    [auth.checkIfLoggedIn, auth.checkIfRightUser], (req, res) => {
      User.findOne({
        where: {
          id: req.params.userId,
        },
        attributes: ['id', 'name', 'email'],
        include: [{
          model: Horse,
        },
        {
          model: Comment,
        }, {
          model: Rating,
          attributes: ['HorseId', 'value'],
        }],
      }).then((user) => {
        res
            .status(200)
            .json({
              name: user.name,
              email: user.email,
            });
      }).catch((err) => {
        res
            .status(500)
            .json({
              err: err.message,
            });
      });
    });

router.put('/:userId',
    [auth.checkIfLoggedIn, auth.checkIfRightUser], (req, res) => {
      const salt = crypto.randomBytes(16).toString('hex');
      const hashedPassword = crypto.pbkdf2Sync(req.body.userPassword, salt,
          10000, 512, 'sha512')
          .toString('hex');

      User.update({
        name: req.body.userName,
        email: req.body.userEmail,
        password: hashedPassword,
        salt: salt,
      }, {
        where: {
          id: req.params.userId,
        },
      }).then(() => {
        return User.findOne({
          where: {
            id: req.params.userId,
          },
        });
      }).then((user) => {
        res
            .status(200)
            .json(user);
      }).catch((err) => {
        res
            .status(500)
            .json({
              err: err.message,
            });
      });
    });

router.delete('/:userId',
    [auth.checkIfLoggedIn, auth.checkIfRightUser], (req, res) => {
      User.destroy({
        where: {
          id: req.params.id,
        },
      }).then((result) => {
        res
            .status(200)
            .json(result);
      }).catch((err) => {
        res
            .status(500)
            .json({
              err: err.message,
            });
      });
    });

module.exports = router;
