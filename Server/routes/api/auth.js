const User = require('../../models/').User;

auth = {
  checkIfLoggedIn: function(req, res, next) {
    if (!req.session.loggedIn) {
      res
          .status(401)
          .json({
            err: 'No User Logged In',
          });
    } else {
      next();
    }
  },

  checkIfRightUser: function(req, res, next) {
    if (req.session.userId != req.params.userId) {
      res
          .status(401)
          .json({
            err: 'Not permitted',
          });
    } else {
      next();
    }
  },

  checkIfFullUserDataProvided: function(req, res, next) {
    if (!req.body.userPassword || !req.body.userName || !req.body.userEmail) {
      res.status(405).json({
        err: 'Missing data fields',
      });
    } else {
      next();
    }
  },

  checkIfImageWasSent: function(req, res, next) {
    if (!req.files || Object.keys(req.files).length === 0) {
      res
          .status(400)
          .json({
            err: 'No image sent',
          });
    } else {
      next();
    }
  },

  checkIfEmailHasValidFormat: function(req, res, next) {
    // eslint-disable-next-line max-len
    const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!emailRegex.test(req.body.userEmail)) {
      res.status(405).json({
        err: 'Invalid format for email',
      });
    } else {
      next();
    }
  },

  checkIfUserExists: async function(req, res, next) {
    const user = await User.findOne({
      where: {
        name: req.body.userName,
      },
    });
    if (!user) {
      res.status(401).json({
        err: 'Wrong username or password',
      });
    } else {
      next();
    }
  },
};

module.exports = auth;
