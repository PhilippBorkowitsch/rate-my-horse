// eslint-disable-next-line new-cap
const router = require('express').Router();
const shortid = require('shortid');
const fs = require('fs');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const Horse = require('../../models').Horse;
const Rating = require('../../models').Rating;
const User = require('../../models').User;
const Comment = require('../../models').Comment;
const auth = require('./auth');

router.post('/',
    [auth.checkIfLoggedIn, auth.checkIfImageWasSent],
    (req, res) => {
      const horseImage = req.files.picturefile;
      const horseFileName = shortid.generate();
      horseImage.mv('./public/img/' + horseFileName + '.png',
          (err) => {
            if (err) {
              res
                  .status(500)
                  .send(err);
            }
            Horse.create({
              image: horseFileName,
              UserId: req.session.userId,
              rating: 0,
              ratingNumber: 0,
            }).then((horse) => {
              return Horse.findOne({
                where: {
                  id: horse.id,
                },
                include: [{
                  model: User,
                  attributes: ['id', 'name', 'email'],
                }, {
                  model: Comment,
                  include: [{
                    model: User,
                    attributes: ['id', 'name', 'email'],
                  }],
                }, {
                  model: Rating,
                  as: 'Ratings',
                }],
              });
            }).then((horse) => {
              res
                  .status(200)
                  .json(horse);
            }).catch((err) => {
              res
                  .status(500)
                  .json({
                    err: err.message,
                  });
            });
          });
    });

router.get('/', (req, res) => {
  Horse.findAll({
    include: [{
      model: User,
      attributes: ['id', 'name', 'email'],
    }, {
      model: Comment,
      include: [{
        model: User,
        attributes: ['id', 'name', 'email'],
      }],
    }, {
      model: Rating,
      as: 'Ratings',
    }],
  }).then((horses) => {
    res
        .status(200)
        .json(horses);
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  });
});

router.get('/:horseId', (req, res) => {
  Horse.findOne({
    where: {
      id: req.params.horseId,
    },
    include: [{
      model: User,
      attributes: ['id', 'name', 'email'],
    }, {
      model: Comment,
      include: [{
        model: User,
        attributes: ['id', 'name', 'email'],
      }],
    }, {
      model: Rating,
      as: 'Ratings',
    }],
  }).then((horse) => {
    res
        .status(200)
        .json(horse);
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  });
});

router.get('/user/:userId', (req, res) => {
  Horse.findAll({
    where: {
      UserId: req.params.userId,
    },
    include: [{
      model: User,
      attributes: ['id', 'name', 'email'],
    }, {
      model: Comment,
      include: [{
        model: User,
        attributes: ['id', 'name', 'email'],
      }],
    }, {
      model: Rating,
      as: 'Ratings',
    }],
  }).then((horses) => {
    res
        .status(200)
        .json(horses);
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  });
});

router.put('/:fileName', (req, res) => {
  Horse.update({
    title: req.body.horseTitle,
  }, {
    where: {
      image: req.params.fileName,
    },
  }).then(() => {
    return Horse.findOne({
      where: {
        image: req.params.fileName,
      },
      include: [{
        model: User,
        attributes: ['id', 'name', 'email'],
      }, {
        model: Comment,
        include: [{
          model: User,
          attributes: ['id', 'name', 'email'],
        }],
      }, {
        model: Rating,
        as: 'Ratings',
      }],
    });
  }).then((result) => {
    res
        .status(200)
        .json(result);
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  });
});

router.delete('/:fileName', (req, res) => {
  Horse.destroy({
    where: {
      image: req.params.fileName,
    },
  }).then((result) => {
    fs.unlink('./public/img/' + req.params.fileName + '.png', (err) => {
      if (err) {
        res
            .status(400)
            .json({
              err: err.message,
            });
      } else {
        res
            .status(200)
            .json(result);
      }
    });
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  });
});

router.post('/:horseId', [auth.checkIfLoggedIn], (req, res) => {
  Rating.findOne({
    where: {
      [Op.and]: [{
        HorseId: req.params.horseId,
      }, {
        UserId: req.session.userId,
      }],
    },
  }).then((rating) => {
    if (rating) {
      throw new Error('User already rated');
    }
    return Horse.findOne({
      where: {
        id: req.params.horseId,
      },
    });
  }).then((horse) => {
    return Rating.create({
      value: req.body.ratingValue,
      HorseId: horse.id,
      UserId: req.session.userId,
    });
  }).then((rating) => {
    return Rating.findAll({
      where: {
        HorseId: rating.HorseId,
      },
    });
  }).then((ratings) => {
    const allRatings = ratings.reduce((add, curr) => {
      add.ratingSum += curr.value;
      add.ratingNumber += 1;
      return add;
    }, {
      ratingSum: 0,
      ratingNumber: 0,
    });
    allRatings.ratingMean = allRatings.ratingSum / allRatings.ratingNumber;
    return Horse.update(
        {
          ratingNumber: allRatings.ratingNumber,
          rating: allRatings.ratingMean,
        },
        {
          where: {
            id: req.params.horseId,
          },
        }
    );
  }).then(() => {
    return Horse.findOne({
      where: {
        id: req.params.horseId,
      },
      include: [{
        model: User,
        attributes: ['id', 'name', 'email'],
      }, {
        model: Comment,
        include: [{
          model: User,
          attributes: ['id', 'name', 'email'],
        }],
      }, {
        model: Rating,
        as: 'Ratings',
      }],
    });
  }).then((horse) => {
    res
        .status(200)
        .json(horse);
  }).catch((err) => {
    res
        .status(500)
        .json({
          err: err.message,
        });
  });
});

module.exports = router;
