const express = require('express');
const session = require('express-session');
const models = require('./models');
const fileUpload = require('express-fileupload');
const path = require('path');
const http = require('http');
const seed = require('./db/seed');

const app = express();

models.sequelize.sync({force: true}).then(() => {
  console.log('Connected to database');
  seed();
}).catch(function(err) {
  console.log(err.message);
});

app.use(express.json());
app.use(fileUpload());
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
}));

app.use(require('./routes'));

app.use(express.static(__dirname + '/public'));
app.use(express.static(path.join(__dirname,
    'rate-my-horse/dist/rate-my-horse'
)));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname,
      'rate-my-horse/dist/rate-my-horse/index.html'
  ));
});

const port = process.env.PORT | 3000;

// app.get('/', function(req, res) {
//   if (req.session.loggedIn) {
//     res.send('Welcome back, ' + req.session.userName + '!');
//   } else {
//     res.redirect('/login.html');
//   }
//   res.end();
// });

const server = http.createServer(app);
server.listen(port, () => console.log(
    `RateMyHorse listening on port ${port}`
));

// app.listen(port, () => console.log(
// `RateMyHorse listening on port ${port}!`));

