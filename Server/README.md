# RateMyHorse

By: 5180146 and 8132356

## Project overview

### General

RateMyHorse is an image sharing platform. Pictures (of horses) can be uploaded, rated and commented by signed-in users.

The site - that is both, frontend and backend - is served with an express server on port 3000 of the localhost.

### Features

- browse through horses submitted by users
- register/login as a user
- users can:
    - leave comments on horses
    - leave a rating on a horse
    - post new horses

### Build

- run `npm install` in root folder
- run `gulp` in root folder
    - should build and serve front- and backend
    - should test frontend
    - should open site in browser

### Testing

The following tests are implemented:
- 7 e2e tests 
- 5 unit tests

### Technologies used:

- Backend
    - express
    - sqlite
    - sequelize
- Frontend
    - Angular (v8)
    - Bootstrap (v4)
    - Font Awesome

## Sources:

- Alert Component and Alert service taken from: https://jasonwatmore.com/post/2019/06/10/angular-8-user-registration-and-login-example-tutorial
- Other code is own code