const crypto = require('crypto');
const User = require('../models/index').User;
const Horse = require('../models/index').Horse;
const Comment = require('../models/index').Comment;
const Rating = require('../models/index').Rating;

module.exports = seed = () => {
  const salt = crypto.randomBytes(16).toString('hex');
  const hashedPassword = crypto.pbkdf2Sync('test123', salt,
      10000, 512, 'sha512')
      .toString('hex');
  return Promise.all([
    User.create({
      name: 'Torben',
      email: 'tor.ben@gmail.com',
      password: hashedPassword,
      salt: salt,
    }),
    User.create({
      name: 'Jon',
      email: 'jon_william_horse@hotmail.com',
      password: hashedPassword,
      salt: salt,
    }),
    User.create({
      name: 'HorseGirl29',
      email: 'xxx__JENNY<3__xxx@gmail.com',
      password: hashedPassword,
      salt: salt,
    }),
    User.create({
      name: 'Frauke',
      email: 'f.meier@pferdetraum.de',
      password: hashedPassword,
      salt: salt,
    }),
    Horse.create({
      image: 'p_3ddGYL',
      title: 'A horse',
      UserId: 3,
      rating: 0,
      ratingNumber: 0,
    }),
    Horse.create({
      image: 'wwrA89ZD',
      title: 'Secret HOrse',
      UserId: 1,
      rating: 0,
      ratingNumber: 0,
    }),
    Horse.create({
      image: 'PQuLBrun',
      title: '100% Real Horse',
      UserId: 2,
      rating: 0,
      ratingNumber: 0,
    }),
    Horse.create({
      image: 'FruUXJBC',
      title: 'These are my fighting horses',
      UserId: 4,
      rating: 1,
      ratingNumber: 1,
    }),
    Horse.create({
      image: 'SHHnnh4x',
      title: 'Far away horse',
      UserId: 1,
      rating: 0,
      ratingNumber: 0,
    }),
    Horse.create({
      image: 'RnbcCgrS',
      title: 'FiBu',
      UserId: 2,
      rating: 0,
      ratingNumber: 0,
    }),
    Horse.create({
      image: '0-KQA7eV',
      title: 'Hay There',
      UserId: 3,
      rating: 0,
      ratingNumber: 0,
    }),
    Comment.create({
      text: 'nice horse!',
      HorseId: 1,
      UserId: 2,
    }),
    Comment.create({
      text: 'thxx <3',
      HorseId: 1,
      UserId: 3,
    }),
    Comment.create({
      text: 'this horse is look very secret :O',
      HorseId: 2,
      UserId: 3,
    }),
    Comment.create({
      text: 'u sure this is a horse? 🤔',
      HorseId: 3,
      UserId: 1,
    }),
    Comment.create({
      text: 'This is a fake horse!!!!!',
      HorseId: 3,
      UserId: 4,
    }),
    Comment.create({
      text: 'Very beautipul',
      HorseId: 5,
      UserId: 2,
    }),
    Comment.create({
      text: 'I made this :)',
      HorseId: 5,
      UserId: 1,
    }),
    Comment.create({
      text: 'This horse is very nice',
      HorseId: 6,
      UserId: 3,
    }),
    Rating.create({
      value: 1,
      HorseId: 4,
      UserId: 2,
    }),
  ]);
};
